from flask import render_template
import db

def recursivelyWriteNode(htmlPage, nodeId, recursionDepth, maxRecursionDepth):
    recursionDepth=recursionDepth+1
    thisNode = db.getNode(nodeId)

    # testing the following line:
    #htmlPage += "<div class=\"text-white bg-info\" >"
    
    htmlPage = render_template("id.html", nid=nodeId, content=thisNode[1])
    if recursionDepth < maxRecursionDepth:
    ########
        supportingNodes = []
        refutingNodes = []
        supportingNodeIds = db.getIdsOfSupportingNodes(nodeId)
        refutingNodeIds = db.getIdsOfRefutingNodes(nodeId)
        if recursionDepth == 1:
            htmlPage += render_template("unorderedListRootStart.html")
        if refutingNodeIds !=[]:
            htmlPage += render_template("refutingNodesDivStart.html")
        if supportingNodeIds != []:
            htmlPage += render_template("supportingNodesDivStart.html")
            htmlPage += "<br>"
            htmlPage += render_template("divEnd.html")
            htmlPage += render_template("listItemSupportingNodes.html")
            htmlPage += render_template("unorderedListStart.html")
            for supportingNodeId in supportingNodeIds:
                currentNode=db.getNode(supportingNodeId)
                supportingNodes.append(db.getNode(supportingNodeId))
                htmlPage+=recursivelyWriteNode(htmlPage, currentNode[0], recursionDepth, maxRecursionDepth)
            htmlPage += render_template("unorderedListEnd.html")
            htmlPage += render_template("listItemEnd.html")
            #htmlPage += render_template("divEnd.html")
        #else:
        if refutingNodeIds != []:
            htmlPage += "<br>"
            htmlPage += render_template("divEnd.html")
        #if recursionDepth == 1:
        #    htmlPage += render_template("unorderedListRootStart.html")
        if refutingNodeIds != []:
            htmlPage += render_template("listItemRefutingNodes.html")
            htmlPage += render_template("unorderedListStart.html")
            for refutingNodeId in refutingNodeIds:
                currentNode=db.getNode(refutingNodeId)
                refutingNodes.append(db.getNode(refutingNodeId))
                htmlPage+=recursivelyWriteNode(htmlPage, currentNode[0], recursionDepth, maxRecursionDepth)
            htmlPage += render_template("unorderedListEnd.html")
            htmlPage += render_template("listItemEnd.html")
            #htmlPage += render_template("divEnd.html")
        #htmlPage += render_template("unorderedListEnd.html")
        #if recursionDepth == 1:
            #htmlPage += render_template("unorderedListEnd.html")
    htmlPage += "<br>"

    # Testing the following line:
    #htmlPage += "</div>"

    return htmlPage
