from flask import Flask, redirect, url_for, request, render_template
import db
from applogic import recursivelyWriteNode

app = Flask(__name__)


@app.route("/")
def hello():
    return "test"
'''
def recursivelyWriteNodeOld(htmlPage, nodeId, recursionDepth, maxRecursionDepth):
    recursionDepth=recursionDepth+1
    thisNode = db.getNode(nodeId)
    htmlPage += render_template("id.html", nid=nodeId, content=thisNode[1])
    if recursionDepth < maxRecursionDepth:
    ########
        supportingNodes = []
        refutingNodes = []
        supportingNodeIds = db.getIdsOfSupportingNodes(nodeId)
        refutingNodeIds = db.getIdsOfRefutingNodes(nodeId)
        if supportingNodeIds != []:
            for supportingNodeId in supportingNodeIds:
                currentNode=db.getNode(supportingNodeId)
                supportingNodes.append(db.getNode(supportingNodeId))
                htmlPage+=recursivelyWriteNode(htmlPage, currentNode[0], recursionDepth, maxRecursionDepth)

        if refutingNodeIds != []:
            for refutingNodeId in refutingNodeIds:
                currentNode=db.getNode(refutingNodeId)
                refutingNodes.append(db.getNode(refutingNodeId))
                htmlPage+=recursivelyWriteNode(htmlPage, currentNode[0], recursionDepth, maxRecursionDepth)
    return htmlPage
'''

@app.route("/browse")
def browse():
    node_id = request.args.get('id')
    templateToReturn = render_template("browse.html")
    if not node_id:
        allNodes = db.getAllNodes()
        for node in allNodes:
            templateToReturn+=render_template("id.html", nid=node[0], content=node[1])
    else:
        node = db.getNode(node_id)
        templateToReturn+=recursivelyWriteNode(templateToReturn, node_id, 0, 5)
    templateToReturn+=render_template("footer.html")
    return templateToReturn

@app.route("/argue",methods = ['POST'] )
def argue():
    if request.method == 'POST':
        pass
    else:
        return redirect(url_for('browse'))

@app.route("/create",methods = ['POST'] )
def create():
    if request.method == 'POST':
        newContent = request.form.get('content')
        newNodeId = db.createNode(newContent)
        return str(newNodeId)
    else:
        return redirect(url_for('browse'))

@app.route("/createConnToNode", methods = ['POST'])
def createConnToNode():
    fromNodeId = request.form.get('fromNodeId')
    toNodeId = request.form.get('toNodeId')
    claim = request.form.get('claim')
    newConnId = db.createConnToNode(fromNodeId, toNodeId, claim)
    return redirect(request.headers.get("Referer"))

@app.route("/createNewNodeWithConnToNode", methods = ["POST"])
def createNewNodeWithConn():
    content = request.form.get('content')
    toNodeId = request.form.get('toNodeId')
    claim = request.form.get('claim')
    newNodeId = db.createNode(content)
    print(type(content))
    print(type(toNodeId))
    print(type(str(newNodeId[0])))
    newConnId = db.createConnToNode(str(newNodeId[0]), toNodeId, claim)
    return redirect(request.headers.get("Referer"))

if __name__ == "__main__":
    app.run()
    #app.run(host='0.0.0.0')

