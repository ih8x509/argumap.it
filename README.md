# argumap.it

map arguments - an attempt to bring the principal of "don't repeat yourself" to debate.

This app is ugly, vulnerable to every webapp vuln known to man, and did I mention ugly - inside and out. At least for now.

## Setup

Written in Python 3.10.6 with Flask and MySQL Connector (pip3 install mysql.connector)

UI development only done for firefox right now.

Set up your database as per the file db-cmds

## To do

- [x] allow creation of nodes in the UI
- [x] allow linking of nodes in the UI
- [ ] display node linkage in the UI
- [ ] implement and display connections to connections
- [ ] disallow creation of duplicate nodes
- [ ] make database creds not hard coded
- [ ] implement sqli protection
- [ ] implement xss protection
- [ ] comment the code
- [ ] implement spam protection (yeah....)
- [ ] consider better backend (graphql?)
- [ ] identify an existing library/solution for front end
