import mysql.connector

#global mysqlUser = 'user'
#global mysqlPassword = 'argumap1t'
#global myHost = 'localhost'
#global myDatabase = 'argumapit'

def getAllNodes():
    conn = mysql.connector.connect(user = 'user', password = 'argumap1t', host = '127.0.0.1', database = 'argumapit')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM nodes")
    conn.close()
    return cursor.fetchall()

def getNode(node_id):
    conn = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = conn.cursor()
    #query = "SELECT content FROM nodes WHERE node_id = ?"
    #args = (node_id)
    #cursor.execute(query, args)
    #cursor.execute("SELECT content FROM nodes WHERE node_id = `%s`" % node_id)
    
    # Attempt to sanitize input based on https://stackoverflow.com/questions/3617052/escape-string-python-for-mysql:
    #sql = "SELECT node_id,content FROM nodes WHERE node_id = %s"
    #cursor.execute(sql, (node_id))
    
    node_id=int(node_id)

    cursor.execute("SELECT node_id,content FROM nodes WHERE node_id = \"%s\"" % node_id)
    #print(cursor.fetchone())
    conn.close()
    return cursor.fetchone()

def createNode(newContent):
    conn = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = conn.cursor()
    
    #newContent = conn.convert.escape(newContent)

    cursor.execute("INSERT INTO nodes (content) VALUES (\"%s\")" % newContent)
    cursor.execute("SELECT LAST_INSERT_ID()")
    toReturn = cursor.fetchone()
    conn.commit()
    conn.close()
    return toReturn

def createConnToNode(fromNodeId, toNodeId, claim):
    dbconnection = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = dbconnection.cursor()
    cursor.execute("INSERT INTO connections (from_node_id, to_node_id, claim) VALUES (\"%s\", \"%s\",\"%s\")" % ( fromNodeId, toNodeId, claim))
    cursor.execute("SELECT LAST_INSERT_ID()")
    toReturn = cursor.fetchone()
    dbconnection.commit()
    dbconnection.close()
    return toReturn

def getAllConnectionsToNode(nodeId):
    dbconnection = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = dbconnection.cursor()
    cursor.execute("SELECT conn_id, from_node_id, claim FROM connections WHERE to_node_id=\"%s\"" % nodeId)
    toReturn = cursor.fetchall()
    dbconnection.close()
    return toReturn

# The below function takes a nodeId and returns
# a list of nodeIds that support the claim of the
# provided nodeId.
def getIdsOfSupportingNodes(nodeId):
    dbconnection = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = dbconnection.cursor()
    cursor.execute("SELECT from_node_id FROM connections WHERE to_node_id=\"%s\" AND claim=1;" % nodeId)
    toReturn = []
    for supportingNodeId in cursor:
        toReturn.append(supportingNodeId[0])
    dbconnection.close()
    return toReturn

def getIdsOfRefutingNodes(nodeId):
    dbconnection = mysql.connector.connect(user = 'user', password = 'argumap1t', host = 'localhost', database = 'argumapit')
    cursor = dbconnection.cursor()
    cursor.execute("SELECT from_node_id FROM connections WHERE to_node_id=\"%s\" AND claim=0;" % nodeId)
    toReturn = []
    for refutingNodeId in cursor:
        toReturn.append(refutingNodeId[0])
    #toReturn = cursor.fetchall()
    dbconnection.close()
    return toReturn
